/**
 * @fileOverview CalculatorView View Module File
 */
define(function(require, module, exports) {
    'use strict';

    var $ = require('jquery');

    // Classes
    var CLASS = {
        /**
         * Class to identify calculator form
         *
         * @constant
         * @type {string}
         */
        CALCULATOR_FORM: '.js-calculator-form',

        /**
         * Class to identify the promotion type select list
         *
         * @constant
         * @type {string}
         */
        CALCULATOR_PROMOTION_TYPE_SELECT: '.js-promotion-type-select',

        /**
         * Class to identify the term select list
         *
         * @constant
         * @type {string}
         */
        CALCULATOR_TERM_SELECT: '.js-terms-select',

        /**
         * Class to identify the deferred term input
         *
         * @constant
         * @type {string}
         */
        CALCULATOR_DEFERRED_INPUT: '.js-deferred-input',

        /**
         * Class to identify the deferred term input section
         *
         * @constant
         * @type {string}
         */
        CALCULATOR_DEFERRED_SECTION: '.js-deferred-section',

        /**
         * Class to identify the apr input
         *
         * @constant
         * @type {string}
         */
        CALCULATOR_APR_INPUT: '.js-apr-input',

        /**
         * Class to identify the promotion type input
         *
         * @constant
         * @type {string}
         */
        CALCULATOR_TYPE_INPUT: '.js-promotion-type-input',

        /**
         * Class to identify the compound period input
         *
         * @constant
         * @type {string}
         */
        CALCULATOR_COMPOUND_PERIOD_INPUT: '.js-CompoundingPeriodsPerYear',

        /**
         * Class to identify the calculation results area
         *
         * @constant
         * @type {string}
         */
        CALCULATION_RESULTS: '.js-calculation-results-section',

        /**
         * Class to identify the calculated APR
         *
         * @constant
         * @type {string}
         */
        RESULT_APR: '.js-apr-result',

        /**
         * Class to identify the calculated finance chage
         *
         * @constant
         * @type {string}
         */
        FINANCE_CHARGE: '.js-finance-charge-result',

        /**
         * Class to identify the calculated finance amount
         *
         * @constant
         * @type {string}
         */
        FINANCE_AMT_RESULT: '.js-finance-amt-result',

        /**
         * Class to identify the calculated total payments
         *
         * @constant
         * @type {string}
         */
        PAYMENT_TOTAL_RESULT: '.js-payment-total-result',

        /**
         * Class to identify the calculated sale price
         *
         * @constant
         * @type {string}
         */
        TOTAL_SALESPRICE_RESULT: '.js-total-sales-price-result',

        /**
         * Class to identify the calculated number of payments
         *
         * @constant
         * @type {string}
         */
        NUMBER_OF_PAYMENTS_RESULT: '.js-number-of-payments-result',

        /**
         * Class to identify the calculated payment amount
         *
         * @constant
         * @type {string}
         */
        PAYMENT_AMOUNT_RESULT: '.js-payment-amount-result',

        /**
         * Class to identify the error message section
         *
         * @constant
         * @type {string}
         */
        ERROR_MESSAGE: '.js-calculator-error'
    };

    var CalculatorView = function($element) {
        /**
         * View's element
         *
         * @property $element
         * @type {jQuery}
         */
        this.$element = $element;

        this.init();
    };

    /**
     * Initializes the UI Component View
     * Runs a single setupHandlers call, followed by createChildren and layout
     *
     * @method init
     * @chainable
     */
    CalculatorView.prototype.init = function() {
        /**
         * Flag to indicate whether the module has been enabled
         *
         * @property isEnabled
         * @type {Boolean}
         * @default false
         */
        this.isEnabled = false;

        /**
         * Input form
         *
         * @property $inputform
         * @type {jQuery}
         * @default null
         */
        this.$inputForm = null;

        /**
         * Input type from input form
         *
         * @property $promotionTypeSelectList
         * @type {jQuery}
         * @default null
         */
        this.$promotionTypeSelectList = null;

        /**
         * Term Select List input
         *
         * @property $termSelectList
         * @type {jQuery}
         * @default null
         */
        this.$termSelectList = null;

        /**
         * TODO: YUIDoc_comment
         *
         * @property promotionTypeInput
         * @type {jQuery}
         * @default null
         */
        this.$promotionTypeInput = null;
        /**
         * TODO: YUIDoc_comment
         *
         * @property $deferredSection
         * @type {jQuery}
         * @default null
         */
        this.$deferredSection = null;
        /**
         * Deferred term input
         *
         * @property $deferredInput
         * @type {jQuery}
         * @default null
         */
        this.$deferredInput = null;

        /**
         * Apr input
         *
         * @property $deferredInput
         * @type {jQuery}
         * @default null
         */
        this.$aprInput = null;

        /**
         * Compound Period input
         *
         * @property $deferredInput
         * @type {jQuery}
         * @default null
         */
        this.$compoundPeriods = null;

        /**
         * Section for Output of calculated results
         *
         * @property $resultsSection
         * @type {jQuery}
         * @default null
         */
        this.$resultsSection = null;


        /**
         * APR result field
         *
         * @property $aprResult
         * @type {jQuery}
         * @default null
         */
        this.$aprResult = null;

        /**
         * Financing charge result field
         *
         * @property $financingChargeResult
         * @type {jQuery}
         * @default null
         */
        this.$financingChargeResult = null;

        /**
         * Financing amount result field
         *
         * @property $financingAmountResult
         * @type {jQuery}
         * @default null
         */
        this.$financingAmountResult = null;

        /**
         * Payment total result field
         *
         * @property $paymentTotalResult
         * @type {jQuery}
         * @default null
         */
        this.$paymentTotalResult = null;


        /**
         * total sales price result field
         *
         * @property $totalSalesPriceResult
         * @type {jQuery}
         * @default null
         */
        this.$totalSalesPriceResult = null;

        /**
         * number of payments result field
         *
         * @property $numberOfPaymentsResult
         * @type {string}
         * @default null
         */
        this.$numberOfPaymentsResult = null;

        /**
         * Payment amount result field
         *
         * @property $paymentAmountResult
         * @type {string}
         * @default null
         */
        this.$paymentAmountResult = null;

        /**
         * Error message section
         *
         * @property $errorMessageSection
         * @type {string}
         * @default null
         */
        this.$errorMessageSection = null;

        return this.setupHandlers()
                   .createChildren()
                   .layout()
                   .enable();

    };

    /**
     * Binds the scope of any handler functions
     * Should only be run on initialization of the view
     *
     * @method setupHandlers
     * @chainable
     */
    CalculatorView.prototype.setupHandlers = function() {
        this.onFormSubmitHandler = $.proxy(this.onFormSubmit, this);
        this.onPromotionTypeChangeHandler = $.proxy(this.onPromotionTypeChange, this);
        this.onTermChangeHandler = $.proxy(this.onTermChange, this);

        return this;
    };

      /**
     * Create any child objects or references to DOM elements
     * Should only be run on initialization of the view
     *
     * @method createChildren
     * @chainable
     */
    CalculatorView.prototype.createChildren = function() {
        // Input Selectors
        this.$inputForm = this.$element.find(CLASS.CALCULATOR_FORM);
        this.$promotionTypeSelectList = this.$element.find(CLASS.CALCULATOR_PROMOTION_TYPE_SELECT);
        this.$termSelectList = this.$element.find(CLASS.CALCULATOR_TERM_SELECT);
        this.$deferredSection = this.$element.find(CLASS.CALCULATOR_DEFERRED_SECTION);
        this.$deferredInput = this.$element.find(CLASS.CALCULATOR_DEFERRED_INPUT);
        this.$aprInput = this.$element.find(CLASS.CALCULATOR_APR_INPUT);
        this.$compoundPeriods = this.$element.find(CLASS.CALCULATOR_COMPOUND_PERIOD_INPUT);
        this.$promotionTypeInput = this.$element.find(CLASS.CALCULATOR_TYPE_INPUT);

        // Results Selectors
        this.$resultsSection = this.$element.find(CLASS.CALCULATION_RESULTS);
        this.$aprResult = this.$element.find(CLASS.RESULT_APR);
        this.$financingChargeResult = this.$element.find(CLASS.FINANCE_CHARGE);
        this.$financingAmountResult = this.$element.find(CLASS.FINANCE_AMT_RESULT);
        this.$paymentTotalResult = this.$element.find(CLASS.PAYMENT_TOTAL_RESULT);
        this.$totalSalesPriceResult = this.$element.find(CLASS.TOTAL_SALESPRICE_RESULT);
        this.$numberOfPaymentsResult = this.$element.find(CLASS.NUMBER_OF_PAYMENTS_RESULT);
        this.$paymentAmountResult = this.$element.find(CLASS.PAYMENT_AMOUNT_RESULT);
        this.$errorMessageSection = this.$element.find(CLASS.ERROR_MESSAGE);

        return this;
    };

    /**
     * Performs measurements and applys any positioning style logic
     * Should be run anytime the parent layout changes
     *
     * @method layout
     * @chainable
     */
    CalculatorView.prototype.layout = function() {
        return this;
    };

    /**
     * Enables the view
     * Performs any event binding to handlers
     * Exits early if it is already enabled
     *
     * @method enable
     * @chainable
     */
    CalculatorView.prototype.enable = function() {
        if (this.isEnabled) {
            return this;
        }

        this.isEnabled = true;

        this.$inputForm.on('submit',  this.onFormSubmitHandler);
        this.$promotionTypeSelectList.on('change', this.onPromotionTypeChangeHandler);
        this.$termSelectList.on('change', this.onTermChangeHandler);

        return this;
    };

     /**
     * Disables the view
     * Tears down any event binding to handlers
     * Exits early if it is already disabled
     *
     * @method disable
     * @chainable
     */
    CalculatorView.prototype.disable = function() {
        if (!this.isEnabled) {
            return this;
        }

        this.isEnabled = false;

        this.$inputForm.off('submit', this.onFormSubmitHandler);
        this.$promotionTypeSelectList.off('change', this.onPromotionTypeChangeHandler);
        this.$termSelectList.off('change', this.onTermChangeHandler);

        return this;
    };

    /**
     * Destroys the view
     * Tears down any events, handlers, elements
     * Should be called when the object should be left unused
     *
     * @method destroy
     * @chainable
     */
    CalculatorView.prototype.destroy = function() {
        this.disable();

        return this;
    };

    /**
     * onFormSubmitHandler Handler method
     * Handles submitting the cacluator form
     *
     * @method onFormSubmitHandler
     * @param {jQueryEvent} event Submit event
     */
    CalculatorView.prototype.onFormSubmit = function(event) {
        event.stopPropagation();
        event.preventDefault();

        $.ajax({
            method: 'post',
            url: '/api/calculator/GetCalculation',
            data: this.$inputForm.serialize()
        })
        .success(this._displayResults.bind(this))
        .fail(this._displayResultsError.bind(this));

        return false;
    };

    /**
     * onPromotionTypeChangeHandler Handler method
     * Handles when the promotion type changes
     *
     * @method onPromotionTypeChangeHandler
     * @param {jQueryEvent} event Submit event
     */
    CalculatorView.prototype.onPromotionTypeChange = function(event) {
        var target = event.target;

        $.ajax({
            method: 'get',
            url: '/api/calculator/GetPromotion/' + target.value,
        })
        .success(this._updateInputs.bind(this))
        .fail(this._displayResultsError.bind(this));
    };

    /**
     * onTermChange Handler method
     * Handles when the term changes, sets the APR input to the data-apr value of the select option
     *
     * @method onPromotionTypeChangeHandler
     * @param {jQueryEvent} event Submit event
     */
    CalculatorView.prototype.onTermChange = function(event) {
        var target = event.target;
        var selectedOption = target.options[target.selectedIndex];
        if (selectedOption) {
            this.$aprInput.val(selectedOption.dataset.apr);
        }
    };

    /**
     * Displays calculation results based on back end calculations
     *
     * @method _displayResults
     * @param results object
     * @private
     */
    CalculatorView.prototype._displayResults = function(results) {
        // set results
        this.$financingAmountResult.text('$' + results.AmountFinanced);
        this.$aprResult.text(results.Apr + '%');
        this.$financingChargeResult.text('$' + results.FinanceCharge);
        this.$numberOfPaymentsResult.text(results.NumberOfPayments);
        this.$paymentAmountResult.text('$' + results.PaymentAmount);
        this.$paymentTotalResult.text('$' + results.TotalOfPayments);
        this.$totalSalesPriceResult.text('$' + results.TotalSalesPrice);

        // show resutls
        this.$resultsSection.removeClass('isHidden');

        // make sure error message is hidden
        this.$errorMessageSection.addClass('isHidden');
    };

    /**
     * Displays error if back end calculations fail
     *
     * @method _displayResultsError
     * @private
     */
    CalculatorView.prototype._displayResultsError = function() {
        // display error
        this.$errorMessageSection.removeClass('isHidden');

        // hide results
        this.$resultsSection.addClass('isHidden');
    };

    /**
     * Displays calculation results based on back end calculations
     *
     * @method _displayResults
     * @param results object
     * @private
     */
    CalculatorView.prototype._updateInputs = function(results) {
        var self = this;
        console.log(results);
        // set results
        this.$financingAmountResult.val(results.AmountFinanced);
        this.$compoundPeriods.val(results.CompoundingPeriodsPerYear);
        this.$promotionTypeInput.val(results.PromotionType);

        // Update Terms select list
        this.$termSelectList.empty();
        $.each(results.Terms, function() {
            self.$termSelectList
                 .append($('<option></option>')
                 .attr('value',this.Value)
                 .attr('data-apr', this.Apr)
                 .text(this.Name));
        });

        // Set APR based on first term
        console.log(this.$termSelectList);
        var selectedOption = this.$termSelectList[0].options[0];
        if (selectedOption) {
            this.$aprInput.val(selectedOption.dataset.apr);
        }

        // Show the deferred amount if the value is greater than 0
        if (results.MonthsDeffered > 0) {
            this.$deferredInput.val(results.MonthsDeffered);
            this.$deferredSection.removeClass('isHidden');
        } else {
            this.$deferredSection.addClass('isHidden');
        }

        // make sure error message is hidden
        this.$errorMessageSection.addClass('isHidden');
    };

    return CalculatorView;
});
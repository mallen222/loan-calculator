/**
 * Application configuration declaration.
 *
 * This configuration file is shared between the website and the build script so
 * that values don't have to be duplicated across environments. Any non-shared,
 * environment-specific configuration should placed in appropriate configuration
 * files.
 *
 * Paths to vendor libraries may be added here to provide short aliases to
 * otherwise long and arbitrary paths. If you're using bower to manage vendor
 * scripts, running `grunt inject` will automatically add paths aliases as
 * needed.
 *
 * @example
 *     paths: {
 *         'jquery': '../vendor/jquery/jquery',
 *         'jquery-cookie': '../vendor/jquery-cookie/jquery-cookie'
 *     }
 *
 * Shims provide a means of managing dependencies for non-modular, or non-AMD
 * scripts. For example, jQuery UI depends on jQuery, but it assumes jQuery is
 * available globally. Because RequireJS loads scripts asynchronously, jQuery
 * may or may not be available which will cause a runtime error. Shims solve
 * this problem.
 *
 * @example
 *     shim: {
 *         'jquery-cookie': {
 *             deps: ['jquery'],
 *             exports: null
 *          }
 *     }
 */

 requirejs.config({
    // This is just to set a shorter alias for longer paths
    paths: {
        'jquery': 'lib-thirdparty/jquery-2.0.2.min',
        'modernizr': 'lib-thirdparty/modernizr-2.6.2.min',
        'cookie': 'lib-thirdparty/jquery.cookie'
    },

    // This allows us to set dependencies for third-party libraries that do not follow the RequireJS pattern
    shim: {
        'jquery': { exports: '$' },
        'modernizr': { exports: 'Modernizr' }
    },

    waitSeconds: 120
});

﻿using System;
using System.Collections.Generic;
using System.Linq;

using TvmCalculator.Lib.Models;
using TvmCalculator.Lib.Services.Promotions;

namespace TvmCalculator.Lib.Data
{
    /// <summary>
    ///     A Mock object of a data store of Promotions.
    ///     Created for portability of the code review. No database and seed method needed.
    /// </summary>
    public class MockPromotionsDataStore
    {
        private static MockPromotionsDataStore _instance;
        private IEnumerable<Promotion> _data;

        /// <summary>
        ///     Singleton so the data only stays in memory once
        /// </summary>
        private MockPromotionsDataStore()
        {
        }

        /// <summary>
        ///     Gets a singleton of MockOromotionsDataStore
        /// </summary>
        public static MockPromotionsDataStore Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MockPromotionsDataStore();
                }

                return _instance;
            }
        }

        /// <summary>
        ///     Gets Mock Data
        /// </summary>
        public IEnumerable<Promotion> Data
        {
            get
            {
                if (_data == null)
                {
                    _data = BuildMockData();
                }

                return _data;
            }
        }

        /// <summary>
        ///     Builds mock Promotion data in memory
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Promotion> BuildMockData()
        {
            var terms = new List<Term>
            {
                new Term
                {
                    Id = 1,
                    Name = "24 month",
                    Value = 24,
                    Apr = 17.99M
                },
                new Term
                {
                    Id = 2,
                    Name = "36 month",
                    Value = 36,
                    Apr = 16.99M
                },
                new Term
                {
                    Id = 3,
                    Name = "48 month",
                    Value = 48,
                    Apr = 15.99M
                },
                new Term
                {
                    Id = 4,
                    Name = "60 month",
                    Value = 60,
                    Apr = 14.99M
                }
            };

            return new List<Promotion>
            {
                new Promotion
                {
                    Id = 1,
                    Terms = terms,
                    Name = "Standard",
                    CompoundingPeriodsPerYear = 12,
                    MonthsDeffered = 0,
                    PromotionType = PromotionType.Standard
                },
                new Promotion
                {
                    Id = 2,
                    Terms = terms,
                    Name = "Compounded Daily",
                    CompoundingPeriodsPerYear = 365,
                    MonthsDeffered = 0,
                    PromotionType = PromotionType.Standard
                },
                new Promotion
                {
                    Id = 3,
                    Terms = new List<Term>
                    {
                        terms[0],
                        terms[3]
                    },
                    Name = "Three month Deferred",
                    CompoundingPeriodsPerYear = 12,
                    MonthsDeffered = 3,
                    PromotionType = PromotionType.Deferred
                },
                new Promotion
                {
                    Id = 4,
                    Terms = new List<Term>
                    {
                        terms[0],
                        terms[3]
                    },
                    Name = "No Interest, No Payment",
                    CompoundingPeriodsPerYear = 12,
                    MonthsDeffered = 0,
                    PromotionType = PromotionType.NoInterestNoPayment
                },
            };
        }
    }
}
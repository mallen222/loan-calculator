﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TvmCalculator.Lib.Services.Promotions
{
    public enum PromotionType
    {
        Standard,
        SameAsCash,
        NoInterestNoPayment,
        Deferred,
    }
}
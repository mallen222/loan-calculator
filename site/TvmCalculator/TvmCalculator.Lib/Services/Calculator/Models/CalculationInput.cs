﻿using System;
using System.Collections.Generic;
using System.Linq;

using TvmCalculator.Lib.Services.Promotions;

namespace TvmCalculator.Lib.Services.Calculator.Models
{
    public class CalculationInput
    {
        public int PromotionId { get; set; }
        public PromotionType PromotionType { get; set; }
        public decimal AprAsPercent { get; set; }
        public int ContractTerm { get; set; }
        public decimal ContractAmount { get; set; }
        public int DeferredLengthInMonths { get; set; }
        public int CompoundPeriodsPerYear { get; set; }

        public decimal AprAsDecimal {
            get { return AprAsPercent / 100; }
        }
    }
}
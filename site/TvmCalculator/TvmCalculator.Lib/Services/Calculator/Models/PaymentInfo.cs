﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TvmCalculator.Lib.Services.Calculator.Models
{
    public class PaymentInfo
    {
        public decimal Apr { get; set; }
        public decimal FinanceCharge { get; set; }
        public decimal AmountFinanced { get; set; }
        public decimal PaymentAmount { get; set; }
        public int NumberOfPayments { get; set; }
        
        // Internal Calculations
        public decimal TotalOfPayments
        {
            get { return PaymentAmount * NumberOfPayments; }
        }

        public decimal TotalSalesPrice
        {
            get { return TotalOfPayments + FinanceCharge; }
        }
    }
}
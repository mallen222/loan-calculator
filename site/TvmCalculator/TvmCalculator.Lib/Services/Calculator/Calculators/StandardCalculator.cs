﻿using System;
using System.Collections.Generic;
using System.Linq;

using TvmCalculator.Lib.Extensions;
using TvmCalculator.Lib.Services.Calculator.Models;

namespace TvmCalculator.Lib.Services.Calculator.Calculators
{
    /// <summary>
    ///     Calculates normal compounding interest for a loan
    /// </summary>
    public class StandardCalculator : ICalculator
    {
        /// <summary>
        ///     Calculates Payment Info based on CacluationInput sent to calculator
        /// </summary>
        /// <param name="input">CalculationInput object</param>
        /// <returns>PaymentInfo object</returns>
        public PaymentInfo Calculate(CalculationInput input)
        {
            var paymentAmount = GetLoanMonthlyPayment(input);

            return new PaymentInfo
            {
                Apr = input.AprAsPercent,
                NumberOfPayments = input.ContractTerm,
                AmountFinanced = input.ContractAmount,
                PaymentAmount = paymentAmount.ToCurrency(),
                FinanceCharge = 0
            };
        }

        /// <summary>
        ///     Calculates monthly payments based on compounding interest formula.
        /// </summary>
        /// <param name="input">CalculationInput object</param>
        /// <returns>Decimal (rounded to 2 decimal places) representing a monthly payment amount</returns>
        private static decimal GetLoanMonthlyPayment(CalculationInput input)
        {
            var monthlyPayment = (input.ContractAmount * input.AprAsDecimal / input.CompoundPeriodsPerYear)
                                 / (1 - (decimal)Math.Pow((1 + (double)input.AprAsDecimal / input.CompoundPeriodsPerYear), -input.ContractTerm));

            return monthlyPayment.ToCurrency();
        }
    }
}
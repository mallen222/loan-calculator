﻿using System;
using System.Collections.Generic;
using System.Linq;

using TvmCalculator.Lib.Extensions;
using TvmCalculator.Lib.Services.Calculator.Models;

namespace TvmCalculator.Lib.Services.Calculator.Calculators
{
    public class DeferredCalculator : ICalculator
    {
        /// <summary>
        ///     Calculates Payment Info based on CacluationInput sent to calculator
        /// </summary>
        /// <param name="input">CalculationInput object</param>
        /// <returns>PaymentInfo object</returns>
        public PaymentInfo Calculate(CalculationInput input)
        {
            var paymentAmount = GetDeferredMonthlyPayments(input);
            
            return new PaymentInfo
            {
                Apr = input.AprAsPercent,
                NumberOfPayments = input.ContractTerm,
                AmountFinanced = input.ContractAmount,
                PaymentAmount = paymentAmount,
                FinanceCharge = 0
            };
        }

        /// <summary>
        ///     Calculates monthly payment for a deferred loan (first payment is deferred but interest keeps accumulating).
        /// </summary>
        /// <param name="input">CalculationInput object</param>
        /// <returns>Decimal (rounded to 2 decimal places) representing a monthly payment amount</returns>
        private static decimal GetDeferredMonthlyPayments(CalculationInput input)
        {
            // The following logic was provided by the client as a visual foxpro program. 
            // The variables have been renamed to be more clear and duplicated code has been reduced

            if (input.AprAsPercent == 0)
            {
                // No interest, monthly payment is amount / number of payments
                return GetNoInterestMonthlyPayment(input);
            }

            if (input.DeferredLengthInMonths == 0)
            {
                // Not really deferred, return standard compounded interest result
                return GetStandardLoanMonthlyPayment(input);
            }

            // Effective interest rate as decimal, the interest rate divided by compound period give the interest rate for the period
            var effectiveInterestRate = input.AprAsDecimal / input.CompoundPeriodsPerYear;
            // A deferred loan is the original term plus the deferred period.
            // Ex. a 48 month loan + 3 month deferred = a 51 month loan.
            var lengthOfLoan = input.ContractTerm + input.DeferredLengthInMonths;
            // The loop below essentially does a guess and check over different payment amounts until the correct one is found
            // lowPayment and HighPayment becom the lower and upper abounds of what payments will be tested.
            var lowPayment = input.ContractAmount / input.ContractTerm;
            var highPayment = GetStandardLoanMonthlyPayment(input) * (decimal)1.25;

            // Initialize loop vaiables.
            decimal monthlyPayment;

            // The following loop tries different payment amounts until the correct payment amount is found.
            // The correct payment amount is the one that comes closet to adding up to the correct total principal paid
            // It seems like there would be a mathmatical way of determining the correct payment amount through a formula
            // but this is the logic the client asked us to implement.
            // ToCurrency() is a decimal extension use to round a currency value to two decimal places.
            // The client asked us to use rounding in specific places to match the same rounding that will be used to create the loan.
            do
            {
                // Reset loop Variables
                var principalBalance = input.ContractAmount;
                var interestBalanace = 0M;

                // Try a payment amount by averaging the lower and upper bounds, rounded to 2 decimal places
                monthlyPayment = ((lowPayment + highPayment) / 2).ToCurrency();

                // A loop to test the payment amount. 
                // The loop runs through the total lenth of the loan, adding interest and subtacting payments
                for (var i = 1; i < lengthOfLoan; i++)
                {
                    // Interest for this period (loop cycle)
                    var interestForPeriod = (principalBalance * effectiveInterestRate).ToCurrency();
                    // Check wether this period is a deferred period
                    if (i <= input.DeferredLengthInMonths - 1)
                    {
                        // No payment required for this period
                        // Add interest to balance
                        interestBalanace += interestForPeriod;
                    }
                    else
                    {
                        // Payment made this period, reduce interest and principal balance
                        var interestPaid = Math.Min(monthlyPayment, interestBalanace + interestForPeriod);
                        interestBalanace += interestForPeriod - interestPaid;

                        var principalPaid = monthlyPayment - interestPaid;
                        principalBalance -= principalPaid;
                    }
                }

                // Check if this payment was too high or too low and adjust the upper or lower bound
                // This will move the next attempted payment closer to the correct payment.
                if (principalBalance >= 0)
                {
                    lowPayment = monthlyPayment;
                }
                else
                {
                    highPayment = monthlyPayment;
                }

                // Keep looping until the upper and lower bound are within 1 cent or equal. 
                // This means the payment amount has reached equalibrium and is the correct amount.
                // Client business logic says to loop through one more time after the difference is < 0.01 in case
                // the next number rounds down. That's why this is a do-while, not just a while loop.
            } while (Math.Abs(highPayment - lowPayment) > 0.01M);

            return monthlyPayment.ToCurrency();
        }

        /// <summary>
        ///     Calculates monthly payments for a loan using the compound interest formula.
        /// </summary>
        /// <param name="input">CalculationInput object</param>
        /// <returns>Decimal (rounded to 2 decimal places) representing a monthly payment amount</returns>
        private static decimal GetStandardLoanMonthlyPayment(CalculationInput input)
        {
            var monthlyPayment = (input.ContractAmount * input.AprAsDecimal / input.CompoundPeriodsPerYear)
                                 / (1 - (decimal)Math.Pow((1 + (double)input.AprAsDecimal / input.CompoundPeriodsPerYear), -input.ContractTerm));

            return monthlyPayment.ToCurrency();
        }

        /// <summary>
        ///     Calculates monthly payment if interest rate is zero - loan amount divided by total number of months paid.
        /// </summary>
        /// <param name="input">CalculationInput object</param>
        /// <returns>Decimal (rounded to 2 decimal places) representing a monthly payment amount</returns>
        private static decimal GetNoInterestMonthlyPayment(CalculationInput input)
        {
            return (input.ContractAmount / input.ContractTerm).ToCurrency();
        }
    }
}
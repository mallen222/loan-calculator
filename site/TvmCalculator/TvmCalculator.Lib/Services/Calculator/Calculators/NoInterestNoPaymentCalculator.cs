﻿using System;
using System.Collections.Generic;
using System.Linq;

using TvmCalculator.Lib.Extensions;
using TvmCalculator.Lib.Services.Calculator.Models;

namespace TvmCalculator.Lib.Services.Calculator.Calculators
{
    /// <summary>
    ///     No Interest No Payment calculations are similar to a normal compounding interest loan
    ///     but the payments are made at the beginning of the month, causing slightly less interest to accrue.
    /// </summary>
    public class NoInterestNoPaymentCalculator : ICalculator
    {
        /// <summary>
        ///     Calculates Payment Info based on CacluationInput sent to calculator
        /// </summary>
        /// <param name="input">CalculationInput object</param>
        /// <returns>PaymentInfo object</returns>
        public PaymentInfo Calculate(CalculationInput input)
        {
            var monthlyPayment = GetLoanMonthlyPaymentWithAnnuity(input);
            
            return new PaymentInfo
            {
                Apr = input.AprAsPercent,
                NumberOfPayments = input.ContractTerm,
                AmountFinanced = input.ContractAmount,
                PaymentAmount = monthlyPayment,
                FinanceCharge = 0
            };
        }

        /// <summary>
        ///     Calculates the montly payment for compounding interest paid at the beginning of each term.
        /// </summary>
        /// <param name="input">CalculationInput object</param>
        /// <returns>Decimal (rounded to 2 decimal places) representing a monthly payment amount</returns>
        private static decimal GetLoanMonthlyPaymentWithAnnuity(CalculationInput input)
        {
            var monthlyPayment = (input.ContractAmount * input.AprAsDecimal / input.CompoundPeriodsPerYear)
                                 / (1 - (decimal)Math.Pow((1 + (double)input.AprAsDecimal / input.CompoundPeriodsPerYear), -input.ContractTerm))
                                 * (1 / (1 + (input.AprAsDecimal / input.CompoundPeriodsPerYear)));

            return monthlyPayment.ToCurrency();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

using TvmCalculator.Lib.Services.Calculator.Models;

namespace TvmCalculator.Lib.Services.Calculator.Calculators
{
    public interface ICalculator
    {
        PaymentInfo Calculate(CalculationInput input);
    }
}
﻿using TvmCalculator.Lib.Services.Calculator.Models;

namespace TvmCalculator.Lib.Services.Calculator
{
    public interface ICalculatorService
    {
        PaymentInfo CalculatePaymentInfo(CalculationInput input);
    }
}
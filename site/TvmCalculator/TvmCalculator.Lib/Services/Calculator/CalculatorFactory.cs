﻿using System;
using System.Collections.Generic;
using System.Linq;

using TvmCalculator.Lib.Services.Calculator.Calculators;
using TvmCalculator.Lib.Services.Promotions;

namespace TvmCalculator.Lib.Services.Calculator
{
    public class CalculatorFactory
    {
        /// <summary>
        ///     Chooses the correct type of calculator based on promotion type enum
        /// </summary>
        /// <param name="promotionType">Enum PromotionType</param>
        /// <returns>ICalculator calculator</returns>
        public static ICalculator GetCalculator(PromotionType promotionType)
        {
            switch (promotionType)
            {
                case PromotionType.NoInterestNoPayment:
                {
                    return new NoInterestNoPaymentCalculator();
                }
                case PromotionType.Deferred:
                {
                    return new DeferredCalculator();
                }
                default:
                    // PromotionType.Standard or PromotionType.SameAsCash or other
                    return new StandardCalculator();
            }
        }
    }
}
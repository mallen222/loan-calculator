﻿using System;
using System.Collections.Generic;
using System.Linq;

using TvmCalculator.Lib.Services.Calculator.Models;

namespace TvmCalculator.Lib.Services.Calculator
{
    public class CalculatorService : ICalculatorService
    {
        /// <summary>
        ///     Calculates Payment Info based on calculation inputs
        /// </summary>
        /// <param name="input">CalculationInput</param>
        /// <returns>PaymentInfo</returns>
        public PaymentInfo CalculatePaymentInfo(CalculationInput input)
        {
            var calculator = CalculatorFactory.GetCalculator(input.PromotionType);
            var paymentInfo = calculator.Calculate(input);

            return paymentInfo;
        }
    }
}
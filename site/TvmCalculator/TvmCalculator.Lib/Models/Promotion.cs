﻿using System;
using System.Collections.Generic;
using System.Linq;

using TvmCalculator.Lib.Services.Promotions;

namespace TvmCalculator.Lib.Models
{
    public class Promotion
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public PromotionType PromotionType { get; set; }
        public int MonthsDeffered { get; set; }
        public int CompoundingPeriodsPerYear { get; set; }
        public virtual ICollection<Term> Terms { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TvmCalculator.Lib.Models
{
    public class Term
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        public Decimal Apr { get; set; }
    }
}
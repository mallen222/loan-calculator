﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TvmCalculator.Lib.Extensions
{
    public static class DecimalExtensions
    {
        /// <summary>
        ///     Decimal Helper to round a number to two significant figures.
        ///     Ex. (2.1351M).ToCurrency() == 2.14M
        /// </summary>
        /// <param name="d">Decimal</param>
        /// <returns>Dicimal rounded to two significant figures.</returns>
        public static decimal ToCurrency(this decimal d)
        {
            return Math.Round(d, 2);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

using TvmCalculator.Lib.Data;
using TvmCalculator.Lib.Models;

namespace TvmCalculator.Lib.Repositories
{
    /// <summary>
    /// Mock Repository for the sake of this code review. Production code implemented this interface to pull code from a DB.
    /// </summary>
    public class MockPromotionsRepository : IRepository<Promotion>
    {
        public void Create(Promotion entity)
        {
            throw new NotImplementedException();
        }

        public Promotion FindById(int entityId)
        {
            return MockPromotionsDataStore.Instance.Data.FirstOrDefault(p => p.Id == entityId);
        }

        public IEnumerable<Promotion> All()
        {
            return MockPromotionsDataStore.Instance.Data;
        }

        public void Save(Promotion entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Promotion entity)
        {
            throw new NotImplementedException();
        }
    }
}

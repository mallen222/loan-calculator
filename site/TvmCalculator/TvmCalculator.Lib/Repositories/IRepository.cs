﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TvmCalculator.Lib.Repositories
{
    public interface IRepository<T> where T: class
    {
        void Create(T entity);
        T FindById(int entityId);
        IEnumerable<T> All(); 
        void Save(T entity);
        void Delete(T entity);
    }
}

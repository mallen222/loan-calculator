﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace TvmCalculator.Web.Controllers.Api
{
    /// <summary>
    /// TEAM SYRACUS BASE API CONTROLLER
    /// SSE CODE REVIEW NOTE: Not written exclusivley by Mike Allen
    /// </summary>
    public class BaseApiController : ApiController
    {
        private readonly JsonMediaTypeFormatter _format;

        /// <summary>
        /// Creates a new Base Api Controller
        /// </summary>
        public BaseApiController()
        {
            _format = new JsonMediaTypeFormatter();
        }


        /// <summary>
        /// Response with Status Code 200 Ok
        /// </summary>>
        public HttpResponseMessage OkResponse()
        {
            return new HttpResponseMessage()
            {
                StatusCode = System.Net.HttpStatusCode.OK
            };
        }

        /// <summary>
        /// Response with Status Code 200 Ok
        /// </summary>
        /// <typeparam name="T">Model Type</typeparam>
        /// <param name="model">Model Value</param>
        public HttpResponseMessage OkResponse<T>(T model)
        {
            return new HttpResponseMessage()
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Content = new ObjectContent<T>(model, _format)
            };
        }

        /// <summary>
        /// Response with Status Code 500 Bad Request
        /// <remarks>Use when the server throws an exception</remarks>
        /// </summary>
        public HttpResponseMessage BadResponse()
        {
            return new HttpResponseMessage()
            {
                StatusCode = System.Net.HttpStatusCode.InternalServerError,
                Content = new StringContent("An Internal Server Error Occured")
            };
        }

        /// <summary>
        /// Response with Status Code 500 Bad Request
        /// <remarks>Use when the server throws an exception</remarks>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public HttpResponseMessage BadResponse<T>(T model)
        {
            return new HttpResponseMessage()
            {
                StatusCode = System.Net.HttpStatusCode.InternalServerError,
                Content = new ObjectContent<T>(model, _format)
            };
        }

        /// <summary>
        /// Response with Status Code 406 Not Acceptable
        /// <remarks>Use when missing parameter or validation error exists</remarks>
        /// </summary>
        public HttpResponseMessage InvalidResponse()
        {
            return new HttpResponseMessage()
            {
                StatusCode = System.Net.HttpStatusCode.NotAcceptable
            };
        }

        /// <summary>
        /// Response with Status Code 406 Not Acceptable
        /// <remarks>Use when missing parameter or validation error exists</remarks>
        /// </summary>
        public HttpResponseMessage InvalidResponse<T>(T model)
        {
            return new HttpResponseMessage()
            {
                StatusCode = System.Net.HttpStatusCode.NotAcceptable,
                Content = new ObjectContent<T>(model, _format)
            };
        }

    }
}
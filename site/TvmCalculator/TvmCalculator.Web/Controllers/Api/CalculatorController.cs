﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;

using TvmCalculator.Lib.Models;
using TvmCalculator.Lib.Repositories;
using TvmCalculator.Lib.Services.Calculator;
using TvmCalculator.Lib.Services.Calculator.Models;
using TvmCalculator.Web.ViewModels;

namespace TvmCalculator.Web.Controllers.Api
{
    public class CalculatorController : BaseApiController
    {
        private readonly ICalculatorService _calculatorService;
        private readonly IRepository<Promotion> _promotionRepository; 
        public CalculatorController()
        {
            _calculatorService = DependencyResolver.Current.GetService<ICalculatorService>();
            _promotionRepository = DependencyResolver.Current.GetService<IRepository<Promotion>>();
        }

        /// <summary>
        ///     Gets PromotionViewModel by ID
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>PromotionViewModel</returns>
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetPromotion(int id)
        {
            var promotion = _promotionRepository.FindById(id);
            return (promotion == null)
                ? BadResponse("Promotion not found.") 
                : OkResponse(PromotionViewModel.Map(promotion));
        }

        /// <summary>
        ///     Calculates PaymentInfo based on CalculationInput
        /// </summary>
        /// <param name="input">CalculationInput</param>
        /// <returns>PaymentInfo</returns>
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetCalculation(CalculationInput input)
        {
            var paymentInfo = _calculatorService.CalculatePaymentInfo(input);

            return OkResponse(paymentInfo);
        }
    }
}

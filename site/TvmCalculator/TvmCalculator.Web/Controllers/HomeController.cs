﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

using TvmCalculator.Lib.Models;
using TvmCalculator.Lib.Repositories;
using TvmCalculator.Web.ViewModels;

namespace TvmCalculator.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository<Promotion> _promotionRepository;

        public HomeController(IRepository<Promotion> promotionRepository)
        {
            _promotionRepository = promotionRepository;
        }

        /// <summary>
        ///     Gets the calculator page with a list of promotions and a selected initial promotion
        /// </summary>
        /// <returns>ViewResult with model CalculatorInputsViewModel</returns>
        [HttpGet]
        public ViewResult Index()
        {
            var model = new CalculatorInputsViewModel();
            var promotions = _promotionRepository.All().ToList();

            if (promotions.Any())
            {
                var firstPromotion = promotions.First();
                model.PromotionOptions = promotions.Select(p => new SelectListItem
                {
                    Text = p.Name,
                    Value = p.Id.ToString(CultureInfo.InvariantCulture),
                    Selected = (p.Id == firstPromotion.Id)
                });

                model.InitialPromotion = PromotionViewModel.Map(firstPromotion);
            }

            return View(model);
        }
    }
}
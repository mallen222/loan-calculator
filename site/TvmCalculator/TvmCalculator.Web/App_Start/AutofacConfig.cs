﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;

using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;

using TvmCalculator.Lib.Models;
using TvmCalculator.Lib.Repositories;
using TvmCalculator.Lib.Services.Calculator;

namespace TvmCalculator.Web
{
    public class AutofacConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            // Register Controllers
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterFilterProvider();

            // Register application dependencies
            builder.RegisterType<MockPromotionsRepository>()
                .As<IRepository<Promotion>>()
                .InstancePerRequest();

            builder.RegisterType<CalculatorService>()
                .As<ICalculatorService>()
                .InstancePerRequest();

            builder.RegisterFilterProvider();
            var container = builder.Build();

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
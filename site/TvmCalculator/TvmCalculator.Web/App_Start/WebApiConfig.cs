﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace TvmCalculator.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute("API Default", "api/{controller}/{action}/{id}",
                new { action = "index", id = RouteParameter.Optional });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

using TvmCalculator.Lib.Models;

namespace TvmCalculator.Web.ViewModels
{
    public class PromotionViewModel
    {
        public string Name { get; set; }
        public string PromotionType { get; set; }
        public int MonthsDeffered { get; set; }
        public int CompoundingPeriodsPerYear { get; set; }
        public virtual IEnumerable<Term> Terms { get; set; }

        /// <summary>
        ///     Maps a Promotion to a PromotionViewModel
        /// </summary>
        /// <param name="p">Promotion</param>
        /// <returns>PromotionViewModel</returns>
        public static PromotionViewModel Map(Promotion p)
        {
            return new PromotionViewModel
            {
                Name = p.Name,
                PromotionType = p.PromotionType.ToString(),
                MonthsDeffered = p.MonthsDeffered,
                CompoundingPeriodsPerYear = p.CompoundingPeriodsPerYear,
                Terms = p.Terms.ToList()
            };
        }
    }
}
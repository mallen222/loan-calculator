﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace TvmCalculator.Web.ViewModels
{
    public class CalculatorInputsViewModel
    {
        public IEnumerable<SelectListItem> PromotionOptions { get; set; }
        public PromotionViewModel InitialPromotion { get; set; }
    }
}
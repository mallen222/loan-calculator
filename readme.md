#Overview#
This code sample is for a time-value money calculator where users can input loan terms and see what the total cost and monthly payments will be for different types of loans. The calculator was part of the Aqua Finance project and I have expanded and embellished the requirements to make for a more interesting code review.

#Requirements#
- Users can select from a list of promotions that populate a list of available terms, one non editable APR, one non editable Deferred Length (if applicable), and one non editable compound periods per year.

- The calculator will display the final APR, Finance Charge, Financed Amount, Total of Payments, Sales Price, and Payment Amount.

- The Deferred Length field should be hidden unless the promotion type uses deferment.

- The front end will not require any page refreshes for the user to fully use the calculator.

- The front end will use responsive design.

- No form validation is required for this scope.

- Google Chrome on desktop is the only browser in scope.

- The promotions need to be designed to be editable by the client in the future. This scope does not include any means for the client to edit the promotions but they should be architected to load dynamically from a mock data store.

- The following types of TVM calculations need to be developed:
    - Standard TVM
    - No Interest, no payment (payments are made at the beginning of the term instead of the end)
    - Deferred (first payment is deferred by a set number of months, interest accrues during deferment)

- Each promotion will be associated with one of the TVM calculations

- This is the first task in what will grow to be a large project. Create a new project and organize it to allow growth and extensibility. 

#Code#
I developed all the back end code, front end markup and javascript except for the sections listed below. Please judge the markup and JS if you�re able. I�ve worked hard to learn the JS and FE standards so I can contribute to each. 

The following sections were NOT developed by me:

- TvmCalculator.Web.Controllers.Api.BaseApiController is a base controller my small team uses

- The base project files created by Visual Studio

- The FE boilerplate code, CSS reset, and base JS files.
